#include <iostream>
#include <string>
#include "./Headers/Ladder.hpp"

Ladder::Ladder(int startPosition, int endPosition):ladderTop(endPosition),
ladderBottom(startPosition){

}

Ladder::Ladder(){}

int Ladder::getTop() const {
  return ladderTop;
}

int Ladder::getBottom() const {
  return ladderBottom;
}

std::string Ladder::toString() const {
  return std::to_string(this->ladderBottom) + "-" + std::to_string(this->ladderTop);
}
