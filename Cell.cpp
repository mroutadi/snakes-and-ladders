#include <iostream>
#include "./Headers/Cell.hpp"


Cell::Cell(int num) {
  this->cellPos = num;        //Set cell position
  this->isOcc = false;        //is Occuped
  this->isLadBott = false;    //Ladder Bottom checker var
  this->isSnkHead = false;    //snake head checker var
}

Cell::Cell(){}

void Cell::setCellPos(int pos)  {
  this->cellPos = pos;
}

void Cell::setOccupied(bool occupied)  {
  this->isOcc = occupied;
}

bool Cell::isOccupied() const  {
  return this->isOcc;
}

void Cell::setPlayer(Player plr) {
  this->cellPlayer = plr;
}

Player Cell::getPlayer()  const {
  return this->cellPlayer;
}

void Cell::setLadder(Ladder ldr) {
  this->cellLadder = ldr;
  this->isLadBott = true;
}

Ladder Cell::getLadder() const  {
  return this->cellLadder;
}

bool Cell::isLad() const {
  return isLadBott;
}

void Cell::setSnake(Snake snk) {
  this->cellSnake = snk;
  isSnkHead = true;
}

Snake Cell::getSnake() const  {
  return this->cellSnake;
}

bool Cell::isSnk() const {
  return isSnkHead;
}

int Cell::getNum() const {
  return this->cellPos;
}
