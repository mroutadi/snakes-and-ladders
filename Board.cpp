#include <iostream>
#include "./Headers/Board.hpp"


Board::Board(int n):cells(new Cell[n*n]),boardCells(n*n) {
  for (size_t i = 0; i < n*n; i++)
  {
    this->cells[i].setCellPos(i);
  }
}

Board::Board() {}
  
Board::~Board() {
  
}

int Board::numOfCells() const {
  return this->boardCells;
}

void Board::setCellToLadder(int startPosition, int endPosition) {
  Ladder ldr(startPosition, endPosition);     //Create a ladder object
  this->cells[startPosition].setLadder(ldr);  //Make ready Bottom of ladder for placement
  this->ladders.push_back(ldr);               //adding ladder to Board ladders vector
  
}

void Board::setCellToSnake(int headPosition, int tailPosition)  {
  Snake snk(headPosition, tailPosition);      //Create a snake object
  this->cells[headPosition].setSnake(snk);    //Make ready top of snake for placement
  this->snakes.push_back(snk);                //adding snake to Board snake vector
}

std::vector<Ladder> Board::getLadders()
{
  return this->ladders;
}

std::vector<Snake> Board::getSnakes()
{
  return this->snakes;
}

Cell* Board::getCells() {
  return cells;
}