#include <iostream>
#include <vector>
#include <string>
#include "./Headers/Game.hpp"
#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "SFML/System.hpp"


Game::Game(Board board, std::vector<Player> players):gameBoard(board),
gamePlayers(players), numberOfPlayers(players.size()),
toss(0) {
  
}

Game::Game(std::vector<Player> players):gamePlayers(players), 
numberOfPlayers(players.size()),
toss(0) {
  Board board(10);
  this->gameBoard = board;
}

Game::~Game() {

}

void Game::NextDeterminer() {
  if (this->toss+1 == this->gamePlayers.size())
  {
    this->toss = 0;
  }
  else
  {
    this->toss ++;
  }
}

Player Game::currentPlayer() {
  return gamePlayers[this->toss];
}

bool Game::winner() {
  for (Player &i : this->gamePlayers)
  {
    if (i.getPosition() >= 100)
    {
      return true;
    }
  }
  return false;
}

void Game::addPlayer(Player plyr) {
  this->gamePlayers.push_back(plyr);
  this->numberOfPlayers++;
}

void Game::movePlayer(int n) {
  int t = toss;
  int plyrNewPlace = gamePlayers[t].getPosition()+n;
  int lplyrNewPlace = 100-n;

  if (n >=6 && n <= 0)     //check for bad dice
  {
    std::cerr << "Please Use a Standard diy!!" <<std::endl;
  }
  else
  {
    if (plyrNewPlace > 100)                       //Check if player is at last cell
    {
      if (this->getBoard().getCells()[lplyrNewPlace].isOccupied())
      {
        this->getBoard().getCells()[gamePlayers[t].getPosition()].setOccupied(false);
        gamePlayers[t].setPosition(0);
        this->NextDeterminer();
      }
      else
      {
        this->getBoard().getCells()[gamePlayers[t].getPosition()].setOccupied(false);
        gamePlayers[t].setPosition(lplyrNewPlace);
        this->getBoard().getCells()[lplyrNewPlace].setOccupied(true);
        this->NextDeterminer();
      }
    }


    else if (this->getBoard().getCells()[plyrNewPlace].isLad())               //check if is a ladder there
    {
      if (this->getBoard().getCells()[this->getBoard().getCells()[plyrNewPlace].getLadder().getTop()].isOccupied())  //if a player was there
      {
        this->getBoard().getCells()[gamePlayers[t].getPosition()].setOccupied(false);
        gamePlayers[t].setPosition(0);
        this->NextDeterminer();
      }
      else
      {
        this->getBoard().getCells()[gamePlayers[t].getPosition()].setOccupied(false);
        gamePlayers[t].setPosition(this->getBoard().getCells()[plyrNewPlace].getLadder().getTop());
        this->getBoard().getCells()[this->getBoard().getCells()[plyrNewPlace].getLadder().getTop()].setOccupied(true);
        this->NextDeterminer();
      }
    }
    else if (this->getBoard().getCells()[plyrNewPlace].isSnk())               //check if is a snake head
    {
      if (this->getBoard().getCells()[this->getBoard().getCells()[plyrNewPlace].getSnake().getTail()].isOccupied())  //if a player was there
      {
        this->getBoard().getCells()[gamePlayers[t].getPosition()].setOccupied(false);
        gamePlayers[t].setPosition(0);
        this->NextDeterminer();
      }
      else
      {
        this->getBoard().getCells()[gamePlayers[t].getPosition()].setOccupied(false);
        gamePlayers[t].setPosition(this->getBoard().getCells()[plyrNewPlace].getSnake().getTail());
        this->getBoard().getCells()[this->getBoard().getCells()[plyrNewPlace].getSnake().getTail()].setOccupied(true);
        this->NextDeterminer();
      }
    }
    else if (this->getBoard().getCells()[n + gamePlayers[t].getPosition()].isOccupied())        //check if isnt snakeHead or ladderBottom and there is a player
    {
      this->getBoard().getCells()[gamePlayers[t].getPosition()].setOccupied(false);
      gamePlayers[t].setPosition(0);
      this->NextDeterminer();
    }
    else                              //every thing is ready for move
    {
      this->getBoard().getCells()[gamePlayers[t].getPosition()].setOccupied(false);
      gamePlayers[t].setPosition(plyrNewPlace);
      this->getBoard().getCells()[plyrNewPlace].setOccupied(true);
      this->NextDeterminer();
    }
  }
  
}

bool Game::play(int moveCount) {
  this->movePlayer(moveCount);
  if (this->winner())
  {
    std::cerr << "!!!!!!! SomeOne End The Game !!!!!!!" << std::endl;
    this->showSFML();
    exit(0);
  }
  else
  {
    return false;
  }
  
}

Board Game::getBoard() {
  return this->gameBoard;
}

void Game::showSFML() {

  sf::RenderWindow window(sf::VideoMode(500,550), "Snake and Ladders ID=9712358003");
  
  std::vector <sf::RectangleShape> sfmlCells;
  sf::RectangleShape firstCellInit;
  firstCellInit.setSize(sf::Vector2f(50,50));
  sfmlCells.push_back(firstCellInit);
  int cellsSize = this->getBoard().numOfCells();
  sfmlCells[0].setFillColor(sf::Color(120,20,120));
  sfmlCells[0].setPosition(sf::Vector2f(0,500));
  for (size_t i = 1; i <= cellsSize; i++)
  {
    sf::RectangleShape gh;
    gh.setSize(sf::Vector2f(50,50));
    sfmlCells.push_back(gh);
  }
  int fromTopChecker = 0 ;    //check distance from top


  /***********coloring cells*******************/
  /***********coloring cells*******************/
  /***********coloring cells*******************/
  /***********coloring cells*******************/
  /***********coloring cells*******************/

  for (size_t i = 0; i < cellsSize; i++)//////////////here  we color cells
  {
    if ((((i/10)%2 == 0) && (i%2==0)) || (((i/10)%2 )!= 0 && (i%2)!=0))
    {
      sfmlCells[i+1].setFillColor(sf::Color(255, 255, 255));
      //sfmlCells[i+1].setOutlineColor(sf::Color::Black);
      //sfmlCells[i+1].setOutlineThickness(1);
      sfmlCells[i+1].setPosition(sf::Vector2f(((i)%10)*50,fromTopChecker));
    }
    else
    {
      sfmlCells[i+1].setFillColor(sf::Color(126,192,205));
      //sfmlCells[i+1].setOutlineColor(sf::Color::Black);
      //sfmlCells[i+1].setOutlineThickness(1);
      sfmlCells[i+1].setPosition(sf::Vector2f(((i)%10)*50,fromTopChecker));
    }
    if ((i+1) % 10 == 0)
    {
      fromTopChecker += 50;
    }
  }
  sfmlCells[1].setFillColor(sf::Color(255, 64, 0));
  sf::Font font;
  font.loadFromFile("IRPooya.ttf");
  std::vector <sf::Text> boardNums;
  sf::Text txt0("START", font);
  txt0.setFillColor(sf::Color::Yellow);
  txt0.setOutlineColor(sf::Color::Black);
  txt0.setOutlineThickness(1);
  txt0.setCharacterSize(18);
  txt0.setOrigin(sf::Vector2f(txt0.getGlobalBounds().width/2,0));
  txt0.setPosition(sf::Vector2f(sfmlCells[0].getGlobalBounds().width/2 +8,500+txt0.getCharacterSize()));
  txt0.setRotation(45);
  boardNums.push_back(txt0);
  fromTopChecker = 450;
  int checkRow = 1;
  for (int i = 1; i <= cellsSize; i++)
  {
    
    if (checkRow%2 == 1)
    {
      sf::Text txt;
      txt.setFont(font);
      txt.setString(std::to_string(i));
      boardNums.push_back(txt);
      boardNums[i].setFillColor(sf::Color(160, 160, 160));
      boardNums[i].setOutlineColor(sf::Color::Black);
      boardNums[i].setOutlineThickness(0.8);
      boardNums[i].setCharacterSize(18);
      boardNums[i].setPosition(sf::Vector2f(((i-1)%10)*50+2,fromTopChecker-2));
    }
    else
    {
      sf::Text txt;
      txt.setFont(font);
      txt.setString(std::to_string(i));
      boardNums.push_back(txt);
      boardNums[i].setFillColor(sf::Color(160, 160, 160));
      boardNums[i].setOutlineColor(sf::Color::Black);
      boardNums[i].setOutlineThickness(0.8);
      boardNums[i].setCharacterSize(18);
      boardNums[i].setPosition(sf::Vector2f((((10*checkRow)-i)%10)*50+2,fromTopChecker-2));
    }
    if ((i) % 10 == 0)
    {
      fromTopChecker -= 50;
      checkRow++;
    }
  }

  /************player***************/
  /************player***************/
  /************player***************/
  /************player***************/
  /************player***************/
  /************player***************/
  std::vector<int> sfmlPlyrPlaces;                      //setPlayer PLAces
  for (size_t i = 0; i < this->gamePlayers.size(); i++)
  {
    sfmlPlyrPlaces.push_back(this->gamePlayers[i].getPosition());
  }
  int heiiGHt = 450;
  int placeHolder = 0;
  std::vector <sf::CircleShape> Playerss;
  for (size_t i = 0; i < this->numberOfPlayers; i++)
  {
    sf::CircleShape sfmlPlayer(15);
    Playerss.push_back(sfmlPlayer);
  }
  sf::Color CLRs[6] = { sf::Color::Blue, sf::Color::Red, sf::Color::Green, sf::Color::Yellow, sf::Color::Cyan, sf::Color::Magenta};
  for (int i = 0; i < this->gamePlayers.size() ; i++)
  {
    Playerss[i].setOrigin(sf::Vector2f(Playerss[i].getGlobalBounds().width/2,Playerss[i].getGlobalBounds().height/2));
    Playerss[i].setFillColor(CLRs[i]);
    Playerss[i].setFillColor(CLRs[i]);
    int checkRowff = 0;
    heiiGHt = 450;
    if (sfmlPlyrPlaces[i] == 0)
      {
        Playerss[i].setPosition(sf::Vector2f(70 + placeHolder, 525));
        placeHolder += 35;
      }
    for (int dd = 1; dd <= this->getBoard().numOfCells(); dd++)
    {
      if (dd == sfmlPlyrPlaces[i])
      {
        if (checkRowff%2 == 0)
        {
          Playerss[i].setPosition(sf::Vector2f(((dd-1)%10)*50+30, heiiGHt+30));
        }
        else
        {
          Playerss[i].setPosition(sf::Vector2f((((10*(checkRowff+1))-dd)%10)*50+30, heiiGHt+30));
        }
      }
      
      if ((dd) % 10 == 0)
      {
        heiiGHt -= 50;
        checkRowff++;
      }
    }
  }
  /**********************************************/
  /**********************************************/
  /********Ladder                 show***********/
  /**********************************************/
  /**********************************************/
  /*
  std::vector<int> sfmlSnkTops;
  for (size_t i = 0; i < this->gameBoard.getSnakes().size(); i++)
  {
    sfmlSnkTops.push_back(this->gameBoard.getSnakes()[i].getHead());
    std::cout<<sfmlSnkTops[i]<<std::endl;
  }
  std::vector<int> sfmlSnkBotts;
  for (size_t i = 0; i < this->gameBoard.getSnakes().size(); i++)
  {
    sfmlSnkBotts.push_back(this->gameBoard.getSnakes()[i].getTail());
    std::cout<<sfmlSnkBotts[i]<<std::endl;
  }
  std::vector<int> sfmlLadTops;
  for (size_t i = 0; i < this->gameBoard.getLadders().size(); i++)
  {
    sfmlLadTops.push_back(this->gameBoard.getLadders()[i].getTop());
    std::cout<<sfmlLadTops[i]<<std::endl;
  }
  std::vector<int> sfmlLadBotts;
  for (size_t i = 0; i < this->gameBoard.getLadders().size(); i++)
  {
    sfmlLadBotts.push_back(this->gameBoard.getLadders()[i].getBottom());
    std::cout<<sfmlLadBotts[i]<<std::endl;
  }
*/



  
  sf::Event ev;
  while(window.isOpen()) {
    
  

    while(window.pollEvent(ev)) {
      if (ev.type == sf::Event::Closed) {
       window.close();
      }
    }


    window.clear(sf::Color::Black);
    for (size_t i = 0; i <= 100; i++)
    {
      window.draw(sfmlCells[i]);
    }
    for (size_t i = 0; i <= 100; i++)
    {
      window.draw(boardNums[i]);
    }
    for (size_t i = 0; i < Playerss.size(); i++)
    {
      window.draw(Playerss[i]);
    }
    
    window.display();

  }
}
