#include <iostream>
#include <string>
#include "./Headers/Player.hpp"


Player::Player(std::string name):playerName(name),
playerPos(0) {

}

Player::Player(){}

void Player::setPosition(int position)  {
  this->playerPos = position;
}

int Player::getPosition() const  {
  return this->playerPos;
}

std::string Player::getName() const  {
  return this->playerName;
}

std::string Player::toString() const  {
  return this->playerName + " @ " + std::to_string(this->playerPos);
}
