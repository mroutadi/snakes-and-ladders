#include <iostream>
#include <string>
#include "./Headers/Snake.hpp"


Snake::Snake(int headPosition, int tailPosition):snakeHead(headPosition),
snakeTail(tailPosition) {

}

Snake::Snake(){}

int Snake::getTail()  const {
  return snakeTail;
}

int Snake::getHead() const  {
  return snakeHead;
}

std::string Snake::toString() const  {
  return std::to_string(this->snakeHead) + "-" + std::to_string(this->snakeTail);
}
