
#ifndef GAME_HPP
#define GAME_HPP
#include <vector>
#include <iostream>
#include <string>
#include "Board.hpp"

class Game
{
private:
  Board gameBoard;
  std::vector <Player> gamePlayers;
  int numberOfPlayers;
  int toss;                 //this is for check currentplayer

public:
  Game(Board, std::vector<Player>);
  Game(std::vector<Player>);
  ~Game();
  void NextDeterminer();
  Player currentPlayer();
  bool winner();
  void addPlayer(Player);
  void movePlayer(int);
  bool play(int);
  Board getBoard();
  void showSFML();
};


#endif // GAME_HPP
