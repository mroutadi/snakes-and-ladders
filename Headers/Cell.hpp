
#ifndef CELL_HPP
#define CELL_HPP

#include <iostream>
#include "Player.hpp"
#include "Ladder.hpp"
#include "Snake.hpp"
class Cell  {
private:
  int cellPos;
  bool isOcc;
  bool isLadBott;
  bool isSnkHead;
  Player cellPlayer;
  Ladder cellLadder;
  Snake cellSnake;
  
public:
  Cell(int);
  Cell();
  void setCellPos(int);
  void setOccupied(bool);
  bool isOccupied() const;
  void setLadder(Ladder);
  Ladder getLadder() const;
  bool isLad() const;
  void setSnake(Snake);
  Snake getSnake() const;
  bool isSnk() const;
  void setPlayer(Player);
  Player getPlayer() const;
  int getNum() const;
};

#endif // CELL_HPP
