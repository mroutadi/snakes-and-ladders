
#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <iostream>
#include <string>

class Player {
private:
  std::string playerName;
  unsigned int playerPos;

public:
  Player(std::string);
  Player();
  void setPosition(int);
  int getPosition() const;
  std::string getName() const;
  std::string toString() const;

};

#endif // PLAYER_HPP
