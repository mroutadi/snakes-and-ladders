#ifndef BOARD_HPP
#define BOARD_HPP

#include <iostream>
#include <vector>
#include "Cell.hpp"


class Board  {
private:
  int boardCells;
  Cell* cells;
  std::vector <Ladder> ladders;
  std::vector <Snake> snakes;

public:
  Board(int);
  Board();
  ~Board();
  int numOfCells() const;
  void setCellToLadder(int, int);
  void setCellToSnake(int, int);
  std::vector <Ladder> getLadders();
  std::vector <Snake> getSnakes();
  Cell* getCells();
};


#endif // BOARD_HPP
