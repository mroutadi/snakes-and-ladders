#ifndef SNAKE_HPP
#define SNAKE_HPP
#include <iostream>
#include <string>

class Snake {
private:
  int snakeHead;
  int snakeTail;
public:
  Snake(int, int);
  Snake();
  int getTail() const;
  int getHead() const;
  std::string toString() const;
};

#endif // SNAKE_HPP
