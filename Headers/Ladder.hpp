#ifndef LADDER_HPP
#define LADDER_HPP
#include <iostream>

class Ladder {
private:
  int ladderTop;
  int ladderBottom;

public:
  Ladder(int, int);
  Ladder();
  int getTop() const;
  int getBottom() const;
  std::string toString() const;
};

#endif // LADDER_HPP
